let profile = {
        name: "Ash Ketchum", 
        age: 10,
        pokemon: ["Rayquaza", "Kyogre", "Groudon", "Bulbasaur"],
        friends: { 
            hoenn: ["May", "Max"],
            kanto: ["Brock", "Misty"]
        },
        talk: function() {
            console.log(this.pokemon[0] + "! I choose you!")
        }
    }

console.log(profile);
console.log("Result of dot notation:")
console.log(profile.name)
console.log("Result of square bracket notation:")
console.log(profile.friends)
console.log("Result of talk method:")
profile.talk()

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        let remainingHealth = target.health - this.health
        target.health = remainingHealth
        console.log(`${target.name}'s health is now reduced to ${remainingHealth}`);
            if(target.health <= 0) {
                        console.log(`${target.name} fainted.`)
                    }


                    };

        
        }

        



// Creates new instances of the "Pokemon" object each with their unique properties
let rayquaza = new Pokemon("Rayquaza", 100);
let deoxys = new Pokemon("Deoxys", 105);
let magikarp = new Pokemon ("Magikarp", 106);


console.log(rayquaza);
console.log(deoxys);
console.log(magikarp);
console.log(rayquaza.tackle(deoxys));
console.log(deoxys);
console.log(deoxys.tackle(magikarp));
console.log(magikarp);
console.log(magikarp.tackle(deoxys));
console.log(deoxys);
console.log(rayquaza.tackle(magikarp));
console.log(magikarp);
console.log(magikarp.tackle(rayquaza));
console.log(rayquaza);
console.log(rayquaza.tackle(magikarp));
console.log(magikarp);